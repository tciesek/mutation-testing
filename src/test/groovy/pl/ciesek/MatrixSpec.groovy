package pl.ciesek

import spock.lang.Specification

class MatrixSpec extends Specification {
    def "should create identity matrix"() {
        given:
        def matrix = new Matrix()
        matrix.createIdentityMatrix(size)

        expect:
        matrix.width == size
        matrix.height == size

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                assert matrix.getMatrixValue(i, j) == (i == j ? 1 : 0)
            }
        }

        where:
        size << [1, 2, 3]
    }

    def "should throw invalid dimension exception for different row length"() {
        given:
        def matrix = new Matrix()

        when:
        matrix.setMatrixValues([[1], [1, 2]] as double[][])

        then:
        thrown(InvalidDimensionException.class)
    }

    def "should change matrix value"() {
        given:
        def matrix = new Matrix([[0, 1, 3], [1, 1, 1]] as double[][])

        when:
        matrix.setMatrixValue(1, 2, 11)

        then:
        matrix.getMatrixValue(1, 2) == 11
    }

    def "should return correct size"() {
        given:
        def matrix = new Matrix(values as double[][])

        expect:
        matrix.height == height
        matrix.width == width

        where:
        values                 || height | width
        [[0]]                  || 1      | 1
        [[0, 1, 3], [1, 1, 1]] || 2      | 3
    }

    def "should return determinant for 1x1 matrix"() {
        given:
        def matrix = new Matrix([[value]] as double[][])

        expect:
        matrix.determinant() == value

        where:
        value << [1, 2, 3]
    }

    def "should return determinant for 2x2 matrix"() {
        given:
        def matrix = new Matrix([x, y] as double[][])

        expect:
        matrix.determinant() == det

        where:
        x      | y      || det
        [5, 4] | [7, 2] || -18
        [6, 4] | [3, 2] || 0
    }

    def "should return determinant for 3x3 matrix"() {
        given:
        def matrix = new Matrix([[1, 2, 3], [6, 5, 4], [3, 7, 2]] as double[][])

        expect:
        matrix.determinant() == 63
    }
}