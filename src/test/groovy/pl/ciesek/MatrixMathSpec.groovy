package pl.ciesek

import spock.lang.Specification

class MatrixMathSpec extends Specification {
    def math = MatrixMath.instance


    def "should inverse matrix"() {
        given:
        def matrix = new Matrix(values as double[][])

        when:
        def inverse = math.inverseMatrix(matrix)

        then:
        matrixEquals(inverse, expected as double[][])

        where:
        values                            || expected
        [[8]]                             || [[1]]
        [[4, 7], [2, 6]]                  || [[0.6, -0.7], [-0.2, 0.4]]
        [[1, 2, 3], [0, 1, 4], [5, 6, 0]] || [[-24, 18, 5], [20, -15, -4], [-5, 4, 1]]
    }

    def "should add matrices"() {
        given:
        def m1 = new Matrix(a as double[][])
        def m2 = new Matrix(b as double[][])


        when:
        def inverse = math.matrixAddition(m1, m2)

        then:
        matrixEquals(inverse, expected as double[][])

        where:
        a                | b                || expected
        [[8]]            | [[1]]            || [[9]]
        [[3, 4], [1, 3]] | [[2, 1], [2, 3]] || [[5, 5], [3, 6]]
    }

    def "should subtract matrices"() {
        given:
        def m1 = new Matrix(a as double[][])
        def m2 = new Matrix(b as double[][])


        when:
        def inverse = math.matrixSubtracting(m1, m2)

        then:
        matrixEquals(inverse, expected as double[][])

        where:
        a                | b                || expected
        [[8]]            | [[1]]            || [[7]]
        [[3, 4], [1, 3]] | [[2, 1], [2, 3]] || [[1, 3], [-1, 0]]
    }

    def "should multiply matrices"() {
        given:
        def m1 = new Matrix(a as double[][])
        def m2 = new Matrix(b as double[][])


        when:
        def inverse = math.matrixMultiplication(m1, m2)

        then:
        matrixEquals(inverse, expected as double[][])

        where:
        a                | b                || expected
        [[8]]            | [[3]]            || [[24]]
        [[3, 4], [1, 3]] | [[2, 1], [2, 3]] || [[14, 15], [8, 10]]
    }

    void matrixEquals(IMatrix m1, double[][] m2) {
        assert m1.height == m2.length
        assert m1.width == m2[0].length

        for (int i = 0; i < m1.height; i++) {
            for (int j = 0; j < m1.width; j++) {
                assert Math.abs(m1.getMatrixValue(i, j) - m2[i][j]) < 0.000001
            }
        }
    }
}