package pl.ciesek;

public class MatrixMath implements IMatrixMath {

  private static MatrixMath INSTANCE = new MatrixMath();

  private MatrixMath() {
  }

  public static MatrixMath getInstance() {
    return INSTANCE;
  }

  @Override
  public IMatrix inverseMatrix(IMatrix m1) throws InvalidDimensionException {
    if (m1.getHeight() != m1.getWidth()) {
      throw new InvalidDimensionException();
    }

    double absDet = Math.abs(m1.determinant());

    if (m1.getHeight() == 1) {
      return scalarMultiplication(m1, 1 / absDet);
    }
    var cofactors = matrixCofactors(m1);

    return scalarMultiplication(matrixTransposition(cofactors), 1 / absDet);
  }

  private IMatrix matrixCofactors(IMatrix matrix) throws InvalidDimensionException {
    if (matrix.getHeight() != matrix.getWidth()) {
      throw new InvalidDimensionException();
    }

    int size = matrix.getHeight();
    var cofactors = new Matrix(size);

    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        int sign = (i + j) % 2 == 0 ? 1 : -1;
        cofactors.setMatrixValue(i, j, sign * matrixMinor(i, j, matrix));
      }
    }
    return cofactors;
  }

  @Override
  public IMatrix matrixAddition(IMatrix m1, IMatrix m2) throws InvalidDimensionException {
    if (m1.getWidth() != m2.getWidth() || m1.getHeight() != m2.getHeight()) {
      throw new InvalidDimensionException();
    }

    var sum = new Matrix(m1.getHeight(), m1.getWidth());

    for (int i = 0; i < m1.getHeight(); i++) {
      for (int j = 0; j < m1.getWidth(); j++) {
        sum.setMatrixValue(i, j, m1.getMatrixValue(i, j) + m2.getMatrixValue(i, j));
      }
    }
    return sum;
  }

  @Override
  public IMatrix matrixMultiplication(IMatrix m1, IMatrix m2) throws InvalidDimensionException {
    if (m1.getWidth() != m2.getHeight()) {
      throw new InvalidDimensionException();
    }

    var multiplication = new Matrix(m1.getHeight(), m2.getWidth());

    for (int i = 0; i < multiplication.getHeight(); i++) {
      for (int j = 0; j < multiplication.getWidth(); j++) {
        var sum = 0.0;
        for (int k = 0; k < m2.getHeight(); k++) {
          sum += m1.getMatrixValue(i, k) * m2.getMatrixValue(k, j);
        }
        multiplication.setMatrixValue(i, j, sum);
      }
    }

    return multiplication;
  }

  @Override
  public IMatrix matrixSubtracting(IMatrix m1, IMatrix m2) throws InvalidDimensionException {
    if (m1.getWidth() != m2.getWidth() || m1.getHeight() != m2.getHeight()) {
      throw new InvalidDimensionException();
    }

    var sum = new Matrix(m1.getHeight(), m1.getWidth());

    for (int i = 0; i < m1.getHeight(); i++) {
      for (int j = 0; j < m1.getWidth(); j++) {
        sum.setMatrixValue(i, j, m1.getMatrixValue(i, j) - m2.getMatrixValue(i, j));
      }
    }
    return sum;
  }

  @Override
  public IMatrix matrixTransposition(IMatrix m1) throws InvalidDimensionException {
    var transposition = new Matrix(m1.getWidth(), m1.getHeight());

    for (int i = 0; i < transposition.getHeight(); i++) {
      for (int j = 0; j < transposition.getWidth(); j++) {
        transposition.setMatrixValue(i, j, m1.getMatrixValue(j, i));
      }
    }
    return transposition;
  }

  @Override
  public IMatrix scalarMultiplication(IMatrix m1, double scalar) {
    var newMatrix = new Matrix(m1.getHeight(), m1.getWidth());

    for (int i = 0; i < m1.getHeight(); i++) {
      for (int j = 0; j < m1.getWidth(); j++) {
        newMatrix.setMatrixValue(i, j, scalar * m1.getMatrixValue(i, j));
      }
    }
    return newMatrix;
  }

  double matrixMinor(int row, int column, IMatrix matrix)
      throws InvalidDimensionException {
    int matrixHeight = matrix.getHeight();
    int matrixWidth = matrix.getWidth();

    if (matrixWidth == 1 || matrixHeight != matrixWidth) {
      throw new InvalidDimensionException();
    }

    int minorSize = matrixHeight - 1;
    var minorMatrix = new Matrix(minorSize);

    int minorRow = 0;

    for (int i = 0; i < matrixHeight; i++) {
      if (i == row) {
        continue;
      }
      int minorColumn = 0;
      for (int j = 0; j < matrixWidth; j++) {
        if (j == column) {
          continue;
        }
        minorMatrix.setMatrixValue(minorRow, minorColumn, matrix.getMatrixValue(i, j));
        minorColumn++;
      }
      minorRow++;
    }

    return minorMatrix.determinant();
  }
}
