package pl.ciesek;

import java.util.Arrays;

public class Matrix implements IMatrix {

  private double[][] values;
  private int height;
  private int width;

  private MatrixMath math = MatrixMath.getInstance();

  public Matrix() {
  }

  public Matrix(int size) {
    try {
      setMatrixValues(new double[size][size]);
    } catch (InvalidDimensionException e) {
    }
  }

  public Matrix(int height, int width) {
    try {
      setMatrixValues(new double[height][width]);
    } catch (InvalidDimensionException e) {
    }
  }

  public Matrix(double[][] values) throws InvalidDimensionException {
    setMatrixValues(values);
  }

  @Override
  public void createIdentityMatrix(int size) {
    this.values = new double[size][size];
    height = size;
    width = size;

    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        values[i][j] = i == j ? 1.0 : 0;
      }
    }
  }

  @Override
  public double determinant() throws InvalidDimensionException {
    if (height != width) {
      throw new InvalidDimensionException();
    }

    return det(this);
  }

  private double det(IMatrix matrix) throws InvalidDimensionException {

    if (matrix.getHeight() == 1) {
      return matrix.getMatrixValue(0, 0);
    }

    double sum = 0;

    int i = 0;
    int sign = 1;
    for (int j = 0; j < matrix.getHeight(); j++) {
      sum += sign * matrix.getMatrixValue(i, j) * math.matrixMinor(i, j, matrix);
      sign = -sign;
    }

    return sum;
  }

  @Override
  public double getMatrixValue(int row, int column) {
    return this.values[row][column];
  }

  @Override
  public void setMatrixValue(int row, int column, double value) {
    this.values[row][column] = value;
  }

  @Override
  public void setMatrixValues(double[][] values) throws InvalidDimensionException {
    int newHeight = values.length;
    int newWidth = values[0].length;

    for (int i = 0; i < newHeight; i++) {
      if (values[i].length != newWidth) {
        throw new InvalidDimensionException();
      }
    }
    this.height = newHeight;
    this.width = newWidth;
    this.values = values;
  }

  @Override
  public int getWidth() {
    return width;
  }

  @Override
  public int getHeight() {
    return height;
  }
}
